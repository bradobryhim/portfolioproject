package medical.admission;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class AdmissionsApp extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static final Object PatientCreation = null;
	protected JScrollPane scrollPane;
	protected JPanel contentPane;
	protected JTextField txtSearch;
	protected JTable table;
	protected JDialog dialog;
	private String MRN;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdmissionsApp frame = new AdmissionsApp();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdmissionsApp() {
		setTitle("Search or Create New Patient");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 575, 488);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtSearch = new JTextField();
		txtSearch.setSize(255, 30);
		txtSearch.setLocation(15, 5);
		txtSearch.setHorizontalAlignment(SwingConstants.LEFT);
		txtSearch.setSelectionStart(10);
		txtSearch.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		txtSearch.setText("Search");
		contentPane.add(txtSearch);
		txtSearch.setColumns(10);
		
		String[] searchOptions = {"Last Name", "MRN", "Primary Physician"};
		JComboBox<String> comboBoxSearchOpt = new JComboBox<>(searchOptions);
		comboBoxSearchOpt.setLocation(276, 8);
		comboBoxSearchOpt.setSize(150, 27);
		comboBoxSearchOpt.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		contentPane.add(comboBoxSearchOpt);
		
		JButton btnSearch = new JButton("Patient Search");
		btnSearch.setLocation(432, 7);
		btnSearch.setSize(132, 29);
		contentPane.add(btnSearch);
		
		
		JButton btnNewPatient = new JButton("Create New Patient");
		btnNewPatient.setLocation(396, 422);
		btnNewPatient.setSize(162, 29);
		contentPane.add(btnNewPatient);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(15, 46, 543, 364);
		contentPane.add(scrollPane);
		
		JButton btnViewEdit = new JButton("View / Edit Patient");
		btnViewEdit.setBounds(6, 422, 159, 29);
		contentPane.add(btnViewEdit);
				
		btnNewPatient.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				NewPatientDialog dialog = new NewPatientDialog();
				dialog.setLocationRelativeTo(contentPane);
				dialog.setVisible(true);
			}
		});
		
		btnSearch.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String SearchString = txtSearch.getText();
				String SearchCol = null;
				if (comboBoxSearchOpt.getSelectedItem() == "Last Name")
				{
					SearchCol = "lastName";
				}
				else if (comboBoxSearchOpt.getSelectedItem() == "MRN")
				{
					SearchCol = "MRN";
				}
				else if (comboBoxSearchOpt.getSelectedItem() == "Primary Physician")
				{
					SearchCol = "primaryDoc";
				}
				
				SearchTable.setSearchString(SearchString);
				SearchTable.setSearchCol(SearchCol);
				
				table = new JTable(new SearchTable());
				table.setFillsViewportHeight(true);
				scrollPane.setViewportView(table);
			}
		});
		
		btnViewEdit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (table == null)
				{
					JOptionPane.showMessageDialog(contentPane,
						    "Please search for a patient to view or edit their information.",
						    "View/Edit Patient Error",
						    JOptionPane.ERROR_MESSAGE);
				}
				else if (table.getRowCount() == 0)
				{
					JOptionPane.showMessageDialog(contentPane,
						    "Please select a patient to view or edit their information.",
						    "View/Edit Patient Error",
						    JOptionPane.ERROR_MESSAGE);
				}
				else
				{
					MRN = (String) table.getValueAt(table.getSelectedRow(), 0);
					if (!MRN.isEmpty())
					{
						ViewEditPatient dialog = new ViewEditPatient(MRN);
						dialog.setLocationRelativeTo(contentPane);
						dialog.setVisible(true);			
					}
				}
				
			}
		});
		
	}

}