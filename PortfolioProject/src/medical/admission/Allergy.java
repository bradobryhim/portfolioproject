package medical.admission;

public class Allergy {
	
	protected String allergy;
	protected String dateRecorded;
	
	
	public Allergy(String allergy, String dateRecorded)
	{
		this.allergy = allergy;
		this.dateRecorded = dateRecorded;
	}
}
