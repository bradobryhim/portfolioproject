package medical.admission;

public class Diagnosis {
	
	protected String diagnosis;
	protected String dateRecorded;
	
	public Diagnosis(String diagnosis, String dateRecorded)
	{
		this.diagnosis = diagnosis;
		this.dateRecorded = dateRecorded;
	}

}
