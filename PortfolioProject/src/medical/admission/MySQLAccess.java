package medical.admission;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSetMetaData;
import java.util.Vector;


public class MySQLAccess {
	
	// JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost/portfolio_project?useSSL=false";

	   //  Database credentials
	   static final String USER = "bradobryhim";
	   static final String PASS = "beo13998";
	   
//	   public static String InsertPatient(String firstName, String initial, String lastName, char sex, String birthDate, String socSec, String address1, String address2, String city, String state, String zipCode, String primaryDoc) {
	   public static String InsertPatient(Person patient) {

		   String MRN = null;
		   Connection conn = null;
		   Statement stmt = null;
		   try{
			   //Register JDBC driver
			   Class.forName(JDBC_DRIVER);

			   //Open a connection
			   conn = DriverManager.getConnection(DB_URL,USER,PASS);

			   //Write a patient into the Insert Row			   
			   stmt = conn.createStatement();
			  
			   String SQLquery = "INSERT INTO patients (firstName,initial,lastName,sex,birthDate,socSec,address1,address2,city,state,zipCode,primaryDoc) VALUES ('"
					   + patient.firstName + "','" + patient.initial + "','" + patient.lastName + "','" + patient.sex + "','" + patient.birthDate + "','" + patient.socSec + "','" + patient.address1 + "','" + patient.address2 + "','" + patient.city + "','" + patient.state + "','" + patient.zipCode + "','" + patient.primaryDoc + "')";
			   
			   stmt.executeUpdate(SQLquery,Statement.RETURN_GENERATED_KEYS);

			   ResultSet rs = stmt.getGeneratedKeys();

			   if (rs.next()) {
			        MRN = rs.getString(1);
			   } else 
			   {
				   System.out.println("No key generated.");
			   }



			   
			   //Close all connections
			   rs.close();
			   stmt.close();
			   conn.close();

		   }catch(SQLException se){
			   //Handle errors for JDBC
			   se.printStackTrace();
		   }catch(Exception e){
			   //Handle errors for Class.forName
			   e.printStackTrace();
		   }finally{
			   //finally block used to close resources
			   try{
				   if(stmt!=null)
					   stmt.close();
			   }catch(SQLException se2){
			   }// nothing we can do
			   try{
				   if(conn!=null)
					   conn.close();
			   }catch(SQLException se){
				   se.printStackTrace();
			   }//end finally try
		   }//end try
		   return MRN;
	   }

	   // Queries the mySQL database.  Returns a vector for table building.
	   public static Vector SearchPatient(String SearchString, String SearchCol) {
		   Connection conn = null;
		   Statement stmt = null;
		   ResultSet rs = null;
		   Vector cache = new Vector(1,1);
		   
		   try
		   {
			   //Register JDBC driver
			   Class.forName(JDBC_DRIVER);

			   //Open a connection
			   conn = DriverManager.getConnection(DB_URL,USER,PASS);

			   //Executes a search
			   stmt = conn.createStatement();
			   rs = stmt.executeQuery("SELECT MRN, lastName, firstName, initial, birthDate FROM patients WHERE " + SearchCol + " = " + "'"+ SearchString + "'");
			   
			   //Takes ResultSet data and converts to a Vector			   
			   int colCount = 5;
			   try {
					while (rs.next()) 
					{							
						String[] record = new String[colCount];
				        for (int i = 0; i < colCount; i++) 
				        {
				        	record[i] = rs.getString(i + 1);
				        }
				        cache.addElement(record);
					}
				} catch (SQLException e) 
			   		{
						e.printStackTrace();
			   		}
			
			   // Close all connections			   
			   rs.close();
			   stmt.close();
			   conn.close();
		   }
		   catch (Exception e) {
			   e.printStackTrace();
		   }finally{
			   //finally block used to close resources
			   try{
				   if(conn!=null)
					   conn.close();
			   }
			   catch(SQLException se){
				   se.printStackTrace();
			   }
		   }
		   return cache;
	   }
	   
	   public static Person ViewEditPatient(String MRN){
		   Connection conn = null;
		   Statement stmt = null;
		   ResultSet rs = null;
		   Person person = null;
		   
		   try
		   {
			   //Register JDBC driver
			   Class.forName(JDBC_DRIVER);

			   //Open a connection
			   conn = DriverManager.getConnection(DB_URL,USER,PASS);

			   //Executes a search
			   String SQLquery = "SELECT * FROM patients WHERE MRN = " + "'"+ MRN + "'";
			   stmt = conn.createStatement();
			   rs = stmt.executeQuery(SQLquery);
		   
			   while (rs.next())
			   {
				   String firstName = rs.getString("firstName");
				   String initial = rs.getString("initial");
				   String lastName = rs.getString("lastName");
				   char sex = rs.getString("sex").charAt(0);
				   String birthdate = rs.getString("birthDate");
				   String socSec = rs.getString("socSec");
				   String address1 = rs.getString("address1");
				   String address2 = rs.getString("address2");
				   String city = rs.getString("city");
				   String state = rs.getString("state");
				   String zipCode = rs.getString("zipCode");
				   String primaryDoc = rs.getString("primaryDoc");
				   
				   person = new Person(firstName, initial, lastName, sex, birthdate, socSec, address1, address2, city, state, zipCode, primaryDoc);
			   }
			
			   rs.close();
			   stmt.close();
			   conn.close();
			   
			  
			   
		   } catch (Exception e) {
			   e.printStackTrace();
		   }

		   return person;

	   }
	   
	// Queries the mySQL tables vitals, allergies or diagnoses.  Returns a vector for table building.
	   public static Vector<Object> SearchPatientData(String MRN, String table) {
		   Connection conn = null;
		   Statement stmt = null;
		   ResultSet rs = null;
		   ResultSetMetaData meta = null;
		   Vector<Object> cache = new Vector<Object>(1,1);
		   
		   try
		   {
			   //Register JDBC driver
			   Class.forName(JDBC_DRIVER);

			   //Open a connection
			   conn = DriverManager.getConnection(DB_URL,USER,PASS);

			   //Executes a search
			   stmt = conn.createStatement();
			   
			   //Selects the SQL query based on which table user wants to view
			   String SQLQuery = null;
			   if (table.contains("vitals"))
			   {
				   SQLQuery = "SELECT DATE_FORMAT(dateTaken, '%m-%d-%y') AS dateTaken, height, weight, bpSys, bpDias FROM vitals WHERE MRN =" + MRN;
			   }
			   else if (table.contains("allergies"))
			   {
				   SQLQuery = "SELECT DATE_FORMAT(timestamp, '%m-%d-%y') AS timestamp, allergy FROM allergies WHERE MRN = "+ MRN;
			   }
			   else if (table.contains("diagnoses"))
			   {
				   SQLQuery = "SELECT DATE_FORMAT(timestamp, '%m-%d-%y') AS timestamp, diagnosis FROM diagnoses WHERE MRN = "+ MRN;
			   }
			   
			   rs = stmt.executeQuery(SQLQuery);
			   meta = rs.getMetaData();
			   
			   int colCount = meta.getColumnCount();
			   
			   //Takes ResultSet data and converts to a Vector			   
			   try {
					while (rs.next()) 
					{							
						Vector record = new Vector(colCount);
				        for (int i = 0; i < colCount; i++) 
				        {
				        	record.add(rs.getString(i + 1));
				        }
				        cache.addElement(record);
					}
				} catch (SQLException e) 
			   		{
						e.printStackTrace();
			   		}
			
			   // Close all connections			   
			   rs.close();
			   stmt.close();
			   conn.close();
		   }
		   catch (Exception e) {
			   e.printStackTrace();
		   }finally{
			   //finally block used to close resources
			   try{
				   if(conn!=null)
					   conn.close();
			   }
			   catch(SQLException se){
				   se.printStackTrace();
			   }
		   }
		   return cache;
	   }

	   public static void InsertPatientData(String MRN, String[] data, String table) {
		   Connection conn = null;
		   Statement stmt = null;
//		   ResultSet rs = null;
//		   ResultSetMetaData meta = null;
//		   Vector<Object> cache = new Vector<Object>(1,1);
		   
		   try
		   {
			   //Register JDBC driver
			   Class.forName(JDBC_DRIVER);

			   //Open a connection
			   conn = DriverManager.getConnection(DB_URL,USER,PASS);

			   //Executes a search
			   stmt = conn.createStatement();
			   
			   //Selects the SQL query based on which table user wants to view
			   String SQLQuery = null;
			   if (table.contains("vitals"))
			   {
				   SQLQuery = "INSERT INTO vitals (MRN, height, weight, bpSys, bpDias) VALUES (" + MRN + "," + data[0] + "," + data[1] + "," + data[2] + "," + data[3] + ")";
			   }
			   else if (table.contains("allergies"))
			   {
				   SQLQuery = "INSERT INTO allergies (MRN, allergy) VALUES (" + MRN + ",'" + data[0] + "')";
			   }
			   else if (table.contains("diagnoses"))
			   {
				   SQLQuery = "INSERT INTO diagnoses (MRN, diagnosis) VALUES ("+ MRN + ",'" + data[0] + "')";
			   }
			   
			   stmt.executeUpdate(SQLQuery);
			
			   // Close all connections			   
//			   rs.close();
			   stmt.close();
			   conn.close();
		   }
		   catch (Exception e) {
			   e.printStackTrace();
		   }finally{
			   //finally block used to close resources
			   try{
				   if(conn!=null)
					   conn.close();
			   }
			   catch(SQLException se){
				   se.printStackTrace();
			   }
		   }
	   }
}
