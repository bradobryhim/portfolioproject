package medical.admission;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class NewPatientDialog extends JDialog implements WindowListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textFieldFirstName;
	private JTextField textFieldMiddleInitial;
	private JTextField textFieldLastName;
	private JTextField textFieldAddress1;
	private JTextField textFieldAddress2;
	private JTextField textFieldCity;
	private final ButtonGroup buttonGroupSex = new ButtonGroup();

	/**
	 * Launch the application.  Not needed because AdmissionsApp is handling dialog building.
	 */
//	public static void main(String[] args) {
//		try {
//			NewPatientDialog dialog = new NewPatientDialog();
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 * @param admissionsApp 
	 */
	
	public NewPatientDialog() {
		setTitle("Create New Patient");
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		addWindowListener(this);
		setBounds(100, 100, 530, 305);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblFirstName = new JLabel("First:");
		lblFirstName.setBounds(6, 17, 32, 16);
		contentPanel.add(lblFirstName);
		
		textFieldFirstName = new JTextField();
		textFieldFirstName.setBounds(42, 12, 130, 26);
		contentPanel.add(textFieldFirstName);
		textFieldFirstName.setColumns(10);
		
		JLabel lblMiddleInitial = new JLabel("Init:");
		lblMiddleInitial.setBounds(184, 17, 25, 16);
		contentPanel.add(lblMiddleInitial);
		
		MaskFormatter initial = null;
		try {
			initial = new MaskFormatter("U");
			initial.setPlaceholderCharacter('_');
		} catch (ParseException e) {
			e.printStackTrace();
		}
		textFieldMiddleInitial = new JFormattedTextField(initial);
		textFieldMiddleInitial.setBounds(221, 12, 29, 26);
		contentPanel.add(textFieldMiddleInitial);
		textFieldMiddleInitial.setColumns(10);
		
		JLabel lblLastName = new JLabel("Last:");
		lblLastName.setBounds(259, 17, 70, 16);
		contentPanel.add(lblLastName);
		
		textFieldLastName = new JTextField();
		textFieldLastName.setBounds(300, 12, 141, 26);
		contentPanel.add(textFieldLastName);
		textFieldLastName.setColumns(10);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(6, 74, 518, 12);
		contentPanel.add(separator);
		
		JLabel lblSexmf = new JLabel("Sex:");
		lblSexmf.setBounds(6, 50, 26, 16);
		contentPanel.add(lblSexmf);
		
		JRadioButton rdbtnM = new JRadioButton("M");
		buttonGroupSex.add(rdbtnM);
		rdbtnM.setBounds(34, 47, 43, 23);
		contentPanel.add(rdbtnM);
		
		JRadioButton rdbtnF = new JRadioButton("F");
		buttonGroupSex.add(rdbtnF);
		rdbtnF.setBounds(79, 47, 43, 23);
		contentPanel.add(rdbtnF);
		
		JLabel lblBirthDate = new JLabel("Birth Date: ");
		lblBirthDate.setBounds(124, 50, 70, 16);
		contentPanel.add(lblBirthDate);
		
		DateFormat birthDate = new SimpleDateFormat("dd/MM/yyyy");
		JFormattedTextField textFieldBirthDate = new JFormattedTextField(birthDate);
		textFieldBirthDate.setText("mm/dd/yyyy");
		textFieldBirthDate.setBounds(196, 45, 112, 26);
		contentPanel.add(textFieldBirthDate);
		
		JLabel lblNewLabelSSNum = new JLabel("Social Security #:");
		lblNewLabelSSNum.setBounds(310, 50, 107, 16);
		contentPanel.add(lblNewLabelSSNum);
		
		MaskFormatter SSNum = null;
		try {
			SSNum = new MaskFormatter("###-##-####");
			SSNum.setPlaceholderCharacter('#');
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JFormattedTextField formattedTextFieldSSNum = new JFormattedTextField(SSNum);
		formattedTextFieldSSNum.setText("###-##-####");
		formattedTextFieldSSNum.setBounds(419, 45, 104, 26);
		contentPanel.add(formattedTextFieldSSNum);
		
		JLabel lblNewLabel = new JLabel("Address:");
		lblNewLabel.setBounds(6, 98, 55, 16);
		contentPanel.add(lblNewLabel);
		
		textFieldAddress1 = new JTextField();
		textFieldAddress1.setBounds(75, 93, 410, 26);
		contentPanel.add(textFieldAddress1);
		textFieldAddress1.setColumns(10);
		
		textFieldAddress2 = new JTextField();
		textFieldAddress2.setColumns(10);
		textFieldAddress2.setBounds(75, 125, 410, 26);
		contentPanel.add(textFieldAddress2);
		
		JLabel lblCity = new JLabel("City:");
		lblCity.setBounds(9, 168, 29, 16);
		contentPanel.add(lblCity);
		
		textFieldCity = new JTextField();
		textFieldCity.setBounds(47, 163, 180, 26);
		contentPanel.add(textFieldCity);
		textFieldCity.setColumns(10);
		
		JLabel lblState = new JLabel("State:");
		lblState.setBounds(236, 168, 35, 16);
		contentPanel.add(lblState);
		
		String[] StateList = {"AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI", "WY"};
		JComboBox<String> comboBoxState = new JComboBox<>(StateList);
		comboBoxState.setBounds(280, 163, 77, 27);
		contentPanel.add(comboBoxState);
		
		JLabel lblZipCode = new JLabel("Zip Code:");
		lblZipCode.setBounds(366, 168, 60, 16);
		contentPanel.add(lblZipCode);
		
		MaskFormatter ZipCode = null;
		try {
			ZipCode = new MaskFormatter("#####");
			ZipCode.setPlaceholderCharacter('_');
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JFormattedTextField formattedTextFieldZipCode = new JFormattedTextField(ZipCode);
		formattedTextFieldZipCode.setBounds(435, 163, 60, 26);
		contentPanel.add(formattedTextFieldZipCode);
		
		Vector<String> PhysList = new Vector<String>();
		PhysList.add("Select a Physician");
		PhysList.add("Chakwas");
		PhysList.add("Michel");
		PhysList.add("Muzyka");
		PhysList.add("Solus");
		PhysList.add("Zeschuk");
		PhysList.add("Physician Not Listed");
		
		JComboBox<String> comboBoxPhys = new JComboBox<>(PhysList);
		comboBoxPhys.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				String primaryDoc = (String) comboBoxPhys.getSelectedItem();
				if (primaryDoc.contentEquals("Physician Not Listed"))
				{
					String physicianAdd = JOptionPane.showInputDialog(null,
							"Enter your primary care physician's last name:",
							"Enter your Physician",
							JOptionPane.INFORMATION_MESSAGE);
					PhysList.add(physicianAdd);
					comboBoxPhys.setSelectedItem(physicianAdd);
				}
				else if (primaryDoc.contentEquals("Select a Physician"))
				{
					JOptionPane.showMessageDialog(null,
							"Please select an available physician.",
							"Select a Physician",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		comboBoxPhys.setBounds(158, 212, 184, 27);
		contentPanel.add(comboBoxPhys);
		
		
		JLabel lblPrimaryCarePhysician = new JLabel("Primary Care Physician: ");
		lblPrimaryCarePhysician.setBounds(6, 217, 150, 16);
		contentPanel.add(lblPrimaryCarePhysician);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(6, 196, 518, 12);
		contentPanel.add(separator_2);
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						
						String firstName = textFieldFirstName.getText();
						String initial = textFieldMiddleInitial.getText();
						String lastName = textFieldLastName.getText();
						char sex;
						if (rdbtnM.isSelected())
						{
							sex = 'M';
						}
						else
						{
							sex = 'F';
						}
						String birthDate = textFieldBirthDate.getText();
						String socSec = formattedTextFieldSSNum.getText();
						String address1 = textFieldAddress1.getText();
						String address2 = textFieldAddress2.getText();
						String city = textFieldCity.getText();
						String state = (String) comboBoxState.getSelectedItem();
						String zipCode = formattedTextFieldZipCode.getText();
						String primaryDoc = (String) comboBoxPhys.getSelectedItem();
						
						Person patient = new Person(firstName, initial, lastName, sex, birthDate, socSec, address1, address2, city, state, zipCode, primaryDoc);
						String MRN = MySQLAccess.InsertPatient(patient);
						
						ViewEditPatient dialog = new ViewEditPatient(MRN);
						dialog.setLocationRelativeTo(contentPanel);
						dialog.setVisible(true);
						
						dispose();
						
					}					
				});
				
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) 
					{
						int result = JOptionPane.showConfirmDialog(
					            null,
					            "Your changes have not been saved. Are you sure you want to close this window?",
					            "Unsaved Changes",
					            JOptionPane.YES_NO_OPTION);
					 
					        if (result == JOptionPane.YES_OPTION)
					        {
					            dispose();
					        }

    				}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
		@Override
		public void windowClosing(WindowEvent e)
	    {
//	        JDialog dialog = (JDialog)e.getSource();
			System.out.println("Window is Closing!");
	        int result = JOptionPane.showConfirmDialog(
	            null,
	            "Your changes have not been saved. Are you sure you want to close this window?",
	            "Unsaved Changes",
	            JOptionPane.YES_NO_OPTION);
	 
	        if (result == JOptionPane.YES_OPTION)
	        {
	            dispose();
	        }
	        
	    }

		@Override
		public void windowOpened(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosed(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowActivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}
}
