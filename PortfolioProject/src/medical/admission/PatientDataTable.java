package medical.admission;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

public class PatientDataTable extends AbstractTableModel {
	
	protected static Vector data;
	protected static Vector columnNames;


	public int getColumnCount() {
		return columnNames.size();
	}

	public int getRowCount() {
		return data.size();
	}

	public String getColumnName(int col) {
		return (String) columnNames.get(col);
	}

	public Object getValueAt(int row, int col) {
		return ((Vector) data.get(row)).get(col);
		}

	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	public static void setData(Vector data1) {
		data = data1;
	}

	public static void setColumnNames(Vector columnNames1) {
		columnNames = columnNames1;
	}
	
	

}
