package medical.admission;

import java.time.format.*;
import java.util.ArrayList;

public class Person {
	
	/*
	 * A person has a:
	 * 
	 * First Name
	 * Middle Inital
	 * Last name
	 * Sex
	 * DOB
	 * SS#
	 * MRN
	 * Address
	 * City
	 * State
	 * Zip
	 * Primary Care Physician
	 * Height
	 * Weight
	 * Allergies <ArrayList>
	 * Current conditions <ArrayList>
	 *  
	 */
	
	protected String firstName;
	protected String initial;
	protected String lastName;
	protected char sex;
	protected String birthDate;
	protected String socSec;
	protected int MRN;
	protected String address1;
	protected String address2;
	protected String city;
	protected String state;
	protected String zipCode;
	protected String primaryDoc;

	
	// Constructor
	
	public Person(String firstName, String initial, String lastName, char sex, String birthDate, String socSec, String address1, String address2, String city, String state, String zipCode, String primaryDoc)
	{
		this.firstName = firstName;
		this.initial = initial;
		this.lastName = lastName;
		this.sex = sex;
		this.birthDate = birthDate;
		this.socSec = socSec;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.primaryDoc = primaryDoc;

	}

	@Override
	public String toString() {
		return "Name: " + firstName + " " + initial + " " + lastName + "\nSex: " + sex
				+ "\nBirth Date: " + birthDate + "\nSocial Security #: " + socSec + "\nAddress:\n" + address1
				+ "\n" + address2 + "\n" + city + ", " + state + " " + zipCode + "\nPrimary Physician: " + primaryDoc;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getInitial() {
		return initial;
	}

	public String getLastName() {
		return lastName;
	}

	public char getSex() {
		return sex;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public String getSocSec() {
		return socSec;
	}

	public int getMRN() {
		return MRN;
	}

	public String getAddress1() {
		return address1;
	}
	
	public String getAddress2() {
		return address2;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public String getPrimaryDoc() {
		return primaryDoc;
	}
	

}







