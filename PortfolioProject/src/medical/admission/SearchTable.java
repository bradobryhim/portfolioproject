package medical.admission;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

public class SearchTable extends AbstractTableModel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static String SearchCol;
	protected static String SearchString;
	protected String[] columnNames = {"MRN", "Last Name", "First Name", "Initial", "Date of Birth"};
	protected Vector data = MySQLAccess.SearchPatient(SearchString, SearchCol);

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.size();
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		return ((String[]) data.elementAt(row))[col];
	}

	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	public static void setSearchCol(String searchCol) {
		SearchCol = searchCol;
	}

	public static void setSearchString(String searchString) {
		SearchString = searchString;
	}

}
