package medical.admission;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import java.awt.Rectangle;
import javax.swing.JScrollPane;
import java.awt.Insets;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JSeparator;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.util.Vector;

public class ViewEditPatient extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
//	private JTable table;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		try {
//			ViewEditPatient dialog = new ViewEditPatient(MRN);
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public ViewEditPatient(String MRN) {
		setBounds(100, 100, 380, 548);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		Person patient = MySQLAccess.ViewEditPatient(MRN);
		
		{
			JScrollPane scrollPanePatient = new JScrollPane();
			scrollPanePatient.setBounds(new Rectangle(8, 6, 364, 287));
			contentPanel.add(scrollPanePatient);
			
			JTextArea textArea = new JTextArea("MRN: " + MRN + "\n" + patient.toString());
			textArea.setEditable(false);
			textArea.setMargin(new Insets(10, 10, 10, 10));
			scrollPanePatient.setViewportView(textArea);
		}
		
		String[] DataOptions = {"Vitals", "Diagnoses", "Allergies"};
		JComboBox<String> comboBox = new JComboBox<>(DataOptions);
		comboBox.setLocation(3, 306);
		comboBox.setSize(166, 27);
		contentPanel.add(comboBox);
		
		JButton btnView = new JButton("View");
		btnView.setBounds(172, 305, 100, 29);
		contentPanel.add(btnView);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.setBounds(275, 305, 100, 29);
		contentPanel.add(btnAdd);
		
		JScrollPane scrollPaneData = new JScrollPane();
		scrollPaneData.setBounds(8, 338, 364, 143);
		contentPanel.add(scrollPaneData);
		
		btnView.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String SQLtable = null;
				Vector<Object> colNames = new Vector<Object> ();
				if (comboBox.getSelectedItem().toString().contentEquals("Vitals"))
				{
					SQLtable = "vitals";
					colNames.add("Date Taken");
					colNames.add("Height");
					colNames.add("Weight");
					colNames.add("BP-Dias");
					colNames.add("BP-Sys");
				}
				else if (comboBox.getSelectedItem().toString().contentEquals("Allergies"))
				{
					SQLtable = "allergies";
					colNames.add("Date Recorded");
					colNames.add("Allergy");
				}
				else if (comboBox.getSelectedItem().toString().contentEquals("Diagnoses"))
				{
					SQLtable = "diagnoses";
					colNames.add("Date Recorded");
					colNames.add("Diagnosis");
				}
				PatientDataTable.setData(MySQLAccess.SearchPatientData(MRN, SQLtable));
				PatientDataTable.setColumnNames(colNames);
				
				JTable table = new JTable(new PatientDataTable());
				table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
				scrollPaneData.setViewportView(table);
			}
		});
		
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String SQLtable = null;
				Vector<Object> colNames = new Vector<Object> ();				
				if (comboBox.getSelectedItem().toString().contentEquals("Vitals"))
				{
					NumberFormat vitalsInt = NumberFormat.getIntegerInstance();
				    vitalsInt.setGroupingUsed(false);
				    vitalsInt.setParseIntegerOnly(true);
				    vitalsInt.setMaximumIntegerDigits(3);
				    
					String data[] = new String[4];
					JFormattedTextField Height = new JFormattedTextField(vitalsInt);
					Height.setColumns(5);
				    JFormattedTextField Weight = new JFormattedTextField(vitalsInt);
				    Weight.setColumns(5);
				    JFormattedTextField BPSys = new JFormattedTextField(vitalsInt);
				    BPSys.setColumns(5);
				    JFormattedTextField BPDias = new JFormattedTextField(vitalsInt);
				    BPDias.setColumns(5);
				    JPanel myPanel = new JPanel();
				    myPanel.add(new JLabel("Height:"));
				    myPanel.add(Height);
				    myPanel.add(new JLabel("Weight:"));
				    myPanel.add(Weight);
				    myPanel.add(Box.createHorizontalStrut(15)); // a spacer
				    myPanel.add(new JLabel("BP - Sys:"));
				    myPanel.add(BPSys);
				    myPanel.add(new JLabel("/ Dias:"));
				    myPanel.add(BPDias);

				    int result = JOptionPane.showConfirmDialog(contentPanel, myPanel, 
				    		"Enter Vitals", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
				    
				    if (result == JOptionPane.OK_OPTION || result == JOptionPane.CANCEL_OPTION) {
				    	data[0] = Height.getText();
				    	data[1] = Weight.getText();
				    	data[2] = BPSys.getText();
				    	data[3] = BPDias.getText();
				    }
				    if  (!data[0].isEmpty() && !data[1].isEmpty() && !data[2].isEmpty() && !data[3].isEmpty())
				    {  
						MySQLAccess.InsertPatientData(MRN, data, "vitals");
				    }
					SQLtable = "vitals";
					colNames.add("Date Taken");
					colNames.add("Height");
					colNames.add("Weight");
					colNames.add("BP-Dias");
					colNames.add("BP-Sys");						
				}
				else if (comboBox.getSelectedItem().toString().contentEquals("Allergies"))
				{
					String data[] = new String[1];
					data[0] = (String)JOptionPane.showInputDialog(contentPanel, "Allergy:", "Add a Patient Allergy", JOptionPane.PLAIN_MESSAGE);
					
//					if (!data[0].isEmpty() && data[0]!=null)					
					if (data[0] != null)
					{
						MySQLAccess.InsertPatientData(MRN, data, "allergies");
					}
					SQLtable = "allergies";
					colNames.add("Date Recorded");
					colNames.add("Allergy");		
				}
				else if (comboBox.getSelectedItem().toString().contentEquals("Diagnoses"))
				{
					String data[] = new String[1];
					data[0] = (String)JOptionPane.showInputDialog(contentPanel, "Diagnosis:", "Add a Patient Diagnosis", JOptionPane.PLAIN_MESSAGE);
					if (data[0]!=null)
					{
						MySQLAccess.InsertPatientData(MRN, data, "diagnoses");
					}
						SQLtable = "diagnoses";
						colNames.add("Date Recorded");
						colNames.add("Diagnosis");						

				}
				PatientDataTable.setData(MySQLAccess.SearchPatientData(MRN, SQLtable));
				PatientDataTable.setColumnNames(colNames);
				
				JTable table = new JTable(new PatientDataTable());
				scrollPaneData.setViewportView(table);
				
			}
		});
		
		JSeparator separator = new JSeparator();
		separator.setBounds(5, 292, 370, 12);
		contentPanel.add(separator);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Return to Patient Search");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
