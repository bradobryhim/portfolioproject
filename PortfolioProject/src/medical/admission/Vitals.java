package medical.admission;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Array;

public class Vitals extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		try {
//			Vitals dialog = new Vitals();
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public Vitals(String MRN) {
		setBounds(100, 100, 355, 134);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblHeightinches = new JLabel("Height (inches):");
		lblHeightinches.setBounds(8, 6, 99, 16);
		contentPanel.add(lblHeightinches);
		
		JFormattedTextField formattedTextFieldHeight = new JFormattedTextField();
		formattedTextFieldHeight.setBounds(119, 1, 50, 26);
		contentPanel.add(formattedTextFieldHeight);
		
		JLabel lblWeightpounds = new JLabel("Weight (pounds):");
		lblWeightpounds.setBounds(181, 6, 106, 16);
		contentPanel.add(lblWeightpounds);
		
		JFormattedTextField formattedTextFieldWeight = new JFormattedTextField();
		formattedTextFieldWeight.setBounds(299, 1, 50, 26);
		contentPanel.add(formattedTextFieldWeight);
		
		JLabel lblBpSys = new JLabel("Blood Pressure - Sys:");
		lblBpSys.setBounds(18, 46, 133, 16);
		contentPanel.add(lblBpSys);
		
		JFormattedTextField formattedTextFieldBPSys = new JFormattedTextField();
		formattedTextFieldBPSys.setBounds(167, 41, 50, 26);
		contentPanel.add(formattedTextFieldBPSys);
		
		JLabel lblBPDias = new JLabel("/ Dias:");
		lblBPDias.setBounds(233, 46, 43, 16);
		contentPanel.add(lblBPDias);
		
		JFormattedTextField formattedTextFieldBPDias = new JFormattedTextField();
		formattedTextFieldBPDias.setBounds(292, 41, 50, 26);
		contentPanel.add(formattedTextFieldBPDias);
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						String[] data = new String[4];
						data[0] = formattedTextFieldHeight.getText();
						data[1] = formattedTextFieldWeight.getText();
						data[2] = formattedTextFieldBPSys.getText();
						data[3] = formattedTextFieldBPDias.getText();
						MySQLAccess.InsertPatientData(MRN, data, "vitals");
						dispose();
						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
